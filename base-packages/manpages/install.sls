# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as base_packages with context %}

base-packages-manpages-install-installed:
  pkg.installed:
    - pkgs: {{ base_packages.manpages.pkgs }}

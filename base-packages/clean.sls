# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .dnsutils
  - .fzf.clean
  - .htop.clean
  - .jq.clean
  - .manpages.clean
  - .mtr.clean
  - .ncdu.clean
  - .tree.clean
  - .wget.clean

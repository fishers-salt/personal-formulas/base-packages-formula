# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .dnsutils
  - .fzf
  - .htop
  - .jq
  - .manpages
  - .mtr
  - .ncdu
  - .tree
  - .wget

# frozen_string_literal: true

control 'base-packages-mtr-clean-removed' do
  title 'should not be installed'

  describe package('mtr') do
    it { should_not be_installed }
  end
end

# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch', 'fedora'
    %w[man-db man-pages]
  else
    %w[man-db manpages]
  end

packages.each do |pkg|
  control "base-packages-manpages-clean-#{pkg}-removed" do
    title 'should not be installed'

    describe package(pkg) do
      it { should_not be_installed }
    end
  end
end

# frozen_string_literal: true

control 'base-packages-jq-clean-removed' do
  title 'should not be installed'

  describe package('jq') do
    it { should_not be_installed }
  end
end

# frozen_string_literal: true

control 'base-packages-ncdu-clean-removed' do
  title 'should not be installed'

  describe package('ncdu') do
    it { should_not be_installed }
  end
end

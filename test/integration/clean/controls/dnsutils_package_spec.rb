# frozen_string_literal: true

pkg =
  case os[:name]
  when 'arch'
    'bind'
  when 'fedora'
    'bind-utils'
  else
    'dnsutils'
  end

control 'base-packages-dnsutils-clean-removed' do
  title 'should not be installed'

  describe package(pkg) do
    it { should_not be_installed }
  end
end

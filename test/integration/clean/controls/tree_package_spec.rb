# frozen_string_literal: true

control 'base-packages-tree-clean-removed' do
  title 'should not be installed'

  describe package('tree') do
    it { should_not be_installed }
  end
end

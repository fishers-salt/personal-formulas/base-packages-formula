# frozen_string_literal: true

control 'base-packages-htop-clean-removed' do
  title 'should not be installed'

  describe package('htop') do
    it { should_not be_installed }
  end
end

# frozen_string_literal: true

control 'base-packages-fzf-clean-removed' do
  title 'should not be installed'

  describe package('fzf') do
    it { should_not be_installed }
  end
end

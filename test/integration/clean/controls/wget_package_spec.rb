# frozen_string_literal: true

control 'base-packages-wget-clean-removed' do
  title 'should not be installed'

  describe package('wget') do
    it { should_not be_installed }
  end
end

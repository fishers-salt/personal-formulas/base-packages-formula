# frozen_string_literal: true

pkg =
  case os[:name]
  when 'arch'
    'bind'
  when 'fedora'
    'bind-utils'
  else
    'dnsutils'
  end

control 'base-packages-dnsutils-install-installed' do
  title 'should be installed'

  describe package(pkg) do
    it { should be_installed }
  end
end

# frozen_string_literal: true

packages =
  case os[:name]
  when 'arch', 'fedora'
    %w[man-db man-pages]
  else
    %w[man-db manpages]
  end

packages.each do |pkg|
  control "base-packages-manpages-install-#{pkg}-installed" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end

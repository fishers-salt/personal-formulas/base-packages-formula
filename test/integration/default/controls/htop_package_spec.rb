# frozen_string_literal: true

control 'base-packages-htop-install-installed' do
  title 'should be installed'

  describe package('htop') do
    it { should be_installed }
  end
end

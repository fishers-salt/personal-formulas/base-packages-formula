# frozen_string_literal: true

control 'base-packages-tree-install-installed' do
  title 'should be installed'

  describe package('tree') do
    it { should be_installed }
  end
end

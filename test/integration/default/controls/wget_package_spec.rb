# frozen_string_literal: true

control 'base-packages-wget-install-installed' do
  title 'should be installed'

  describe package('wget') do
    it { should be_installed }
  end
end

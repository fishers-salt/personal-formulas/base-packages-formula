# frozen_string_literal: true

control 'base-packages-jq-install-installed' do
  title 'should be installed'

  describe package('jq') do
    it { should be_installed }
  end
end

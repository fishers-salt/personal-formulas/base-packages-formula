# frozen_string_literal: true

control 'base-packages-ncdu-install-installed' do
  title 'should be installed'

  describe package('ncdu') do
    it { should be_installed }
  end
end

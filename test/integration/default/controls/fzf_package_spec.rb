# frozen_string_literal: true

control 'base-packages-fzf-install-installed' do
  title 'should be installed'

  describe package('fzf') do
    it { should be_installed }
  end
end

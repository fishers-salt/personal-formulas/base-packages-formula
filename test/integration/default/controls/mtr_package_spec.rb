# frozen_string_literal: true

control 'base-packages-mtr-install-installed' do
  title 'should be installed'

  describe package('mtr') do
    it { should be_installed }
  end
end
